resource "azurerm_resource_group" "myterraformgroup" {
  name = "${var.rg_name}"
  location = "${var.location}"
}


resource "azurerm_virtual_network" "myterraformnetwork" {
  name = "${var.avn}"
  address_space = "${var.address}"
  location = "${var.location}"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
}


resource "azurerm_public_ip" "myterraformpublicip" {
  name = "${var.api}"
  location = "${var.location}"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
  public_ip_address_allocation = "dynamic"

}


resource "azurerm_subnet" "myterraformsubnet" {
  name = "${var.as}"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
  virtual_network_name = "${azurerm_virtual_network.myterraformnetwork.name}"
  address_prefix = "10.0.2.0/24"
}



resource "azurerm_network_interface" "myterraformnic" {

  name = "myNIC${count.index}"
  count = "${length(var.ip_addresses)}"
  location = "${var.location}"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"

  ip_configuration {
    name = "myNicConfiguration"
    subnet_id = "${azurerm_subnet.myterraformsubnet.id}"
    private_ip_address_allocation = "static"
    private_ip_address = "${element(var.ip_addresses, count.index)}"
    public_ip_address_id = "${azurerm_public_ip.myterraformpublicip.id}"
  }
}

resource "azurerm_virtual_machine" "myterraformvm" {
  name = "${azurerm_resource_group.myterraformgroup.name}-${count.index}"
  location = "${var.location}"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
  network_interface_ids = [
    "${element(azurerm_network_interface.myterraformnic.*.id, count.index)}"]
  vm_size = "Standard_DS1_v2"
  count = "${length(var.ip_addresses)}"

  storage_os_disk {
    name = "myOsDisk${count.index}"
    caching = "ReadWrite"
    create_option = "FromImage"
    managed_disk_type = "Premium_LRS"

  }

  storage_image_reference {
    publisher = "Canonical"
    offer = "UbuntuServer"
    sku = "16.04.0-LTS"
    version = "latest"
  }

  os_profile {
    computer_name = "myVM"
    admin_username = "stage"
  }

  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path = "/home/stage/.ssh/authorized_keys"
      key_data = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCnYPSLgUS03yPW8P1nv9vH9wrrYRHVgzl6u2XriVyV7Vh4Fzu4GTRlIQtIQhzuX0hKF+wtzCvIWO61g7sWBIDXFPkbZm5rrKl/7XpM/wDRKcvjHHVBglZ/4ztSmXKQQ/B58FL7G4MFzmoj2clokJSsc+oos71haGIxXRfThHvCylbSiVw9+hfLIEG8c3YS5RFaMHs9aYU41Jyz+LMiNHG184hR2npfncu38ZZyJ1lrzGf38YFaTK9VVBFUN5RcCFIJGkmPZpAqDfEtffxP/zJ0/RknB2/hZ/fnRh1heN7iRTOUdLo5/vHfHsWJyucY/VcRYwOvfRHYpoOjfuyoUi2h stage@CentOs.formation"
    }
  }
}



